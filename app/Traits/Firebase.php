<?php

namespace App\Traits;

use Illuminate\Support\Facades\Http;

trait Firebase
{

    public  function firebaseNotification($fcmNotification){

        $fcmUrl = config('firebase.fcm_url');

        $apiKey= 'AAAAANriFg0:APA91bHAJIDxsZ1n9i135VgTG1ft-lMCoHtk8ZkvOj7BQtW6rloAAR7DBIp8zKR6slW9M5uS8cZORqNXHJWZ-5b5ezgMtBdwgmQdeykEnbn_gFPzq6fs3g2PCoz-sveGLw1oa9DB01xS';

        $http=Http::withHeaders([
            'Authorization:key'=>$apiKey,
            'Content-Type'=>'application/json'
        ])  ->post($fcmUrl,$fcmNotification);

        return  $http->json();
    }
}
