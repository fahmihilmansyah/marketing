<?php

namespace App\Http\Livewire\Report\Task;

use Livewire\Component;

class Index extends Component
{
    public function render()
    {
        return view('livewire.report.task.index');
    }
}
