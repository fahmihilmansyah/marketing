<?php

namespace App\Http\Livewire\Tmarket\Task;

use Livewire\Component;

class Index extends Component
{
    public function render()
    {
        return view('livewire.tmarket.task.index');
    }
}
