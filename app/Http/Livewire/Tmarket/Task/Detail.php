<?php

namespace App\Http\Livewire\Tmarket\Task;

use Livewire\Component;

class Detail extends Component
{
    public $tmarket;
    public $queryString=['tmarket'];
    public function render()
    {
        return view('livewire.tmarket.task.detail');
    }
}
