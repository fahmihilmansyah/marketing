<?php

namespace App\Http\Livewire\Tmarket\Task;

use App\Models\datavendor;
use App\Models\master_status;
use App\Models\tmarket_detail;
use Livewire\Component;

class Processtm extends Component
{
    public $nohp,$fullname,$add1,$add2,$add3,$city,$zip,$homeph1,$bussph1,$sex,$dob;
    public $listStatus,$statusId,$desc;
    public $dvi;
    protected $queryString=['dvi'];
    protected $rules=['statusId'=>'required','dvi'=>'required'];
    function mount(){
        $findDatavendor = datavendor::query()->where(['id'=>$this->dvi])->first();
        $findlistStatus = master_status::all();
        $this->listStatus = $findlistStatus;
        if(!empty($findDatavendor)) {
            $this->nohp = $findDatavendor->nohp;
            $this->fullname = $findDatavendor->fullname;
            $this->add1 = $findDatavendor->add1;
            $this->add2 = $findDatavendor->add2;
            $this->add3 = $findDatavendor->add3;
            $this->city = $findDatavendor->city;
            $this->zip = $findDatavendor->zip;
            $this->homeph1 = $findDatavendor->homeph1;
            $this->bussph1 = $findDatavendor->bussph1;
            $this->sex = $findDatavendor->sex;
            $this->dob = $findDatavendor->dob;
        }
    }
    public function render()
    {
        return view('livewire.tmarket.task.processtm');
    }
    public function save(){
        $this->validate();
        $marketdetail = tmarket_detail::query()->where(['datavendor_id'=>$this->dvi])->first();
        $marketid = $marketdetail->tmarket_id;
        $marketdetail->update(['status_id'=>$this->statusId,'desc'=>$this->desc,'updated_at'=>date("Y-m-d H:i:s")]);

        notify()->success('Success adding Response.') ;
        return $this->redirectRoute("detailListTaskTm",['tmarket'=>$marketid]);
    }
}
