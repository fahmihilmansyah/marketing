<?php

namespace App\Http\Livewire\Tmarket\Task;

use App\Models\tmarket_detail;
use App\Models\UserRole;
use Illuminate\Support\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use PowerComponents\LivewirePowerGrid\Rules\{Rule, RuleActions};
use PowerComponents\LivewirePowerGrid\Filters\Filter;
use PowerComponents\LivewirePowerGrid\Traits\ActionButton;
use PowerComponents\LivewirePowerGrid\{Button, Column, Exportable, Footer, Header, PowerGrid, PowerGridComponent, PowerGridEloquent};

final class tmDetailTaskTable extends PowerGridComponent
{
    use ActionButton;
    public $tmarketid;
    /*
    |--------------------------------------------------------------------------
    |  Features Setup
    |--------------------------------------------------------------------------
    | Setup Table's general features
    |
    */
    public function setUp(): array
    {
//        $this->showCheckBox();
        $arrdata =[
            Header::make()->showSearchInput(),
            Footer::make()
                ->showPerPage()
                ->showRecordCount(),
        ];
        $cekrol = UserRole::query()->where(['user_id'=>auth()->user()->id])->first();
        if(empty($cekrol)){
            $arrdata[]=Exportable::make('export')
                ->striped()
                ->type(Exportable::TYPE_XLS, Exportable::TYPE_CSV);
        }
        return $arrdata;
    }

    /*
    |--------------------------------------------------------------------------
    |  Datasource
    |--------------------------------------------------------------------------
    | Provides data to your Table using a Model or Collection
    |
    */

    /**
     * PowerGrid datasource.
     *
     * @return Builder<\App\Models\tmarket_detail>
     */
    public function datasource(): Builder
    {
        return tmarket_detail::query()
            ->select(['tmarket_detail.id','tmarket_detail.datavendor_id','master_status.status_name',
                'tmarket_detail.status_id','datavendor.fullname','datavendor.nohp','datavendor.city',
                'tmarket_detail.created_at'
            ])
            ->join('datavendor','datavendor.id','=','tmarket_detail.datavendor_id')
            ->leftJoin('master_status','master_status.id','=','tmarket_detail.status_id')
            ->where(['tmarket_detail.tmarket_id'=>$this->tmarketid]);
    }

    /*
    |--------------------------------------------------------------------------
    |  Relationship Search
    |--------------------------------------------------------------------------
    | Configure here relationships to be used by the Search and Table Filters.
    |
    */

    /**
     * Relationship search.
     *
     * @return array<string, array<int, string>>
     */
    public function relationSearch(): array
    {
        return [];
    }

    /*
    |--------------------------------------------------------------------------
    |  Add Column
    |--------------------------------------------------------------------------
    | Make Datasource fields available to be used as columns.
    | You can pass a closure to transform/modify the data.
    |
    | ❗ IMPORTANT: When using closures, you must escape any value coming from
    |    the database using the `e()` Laravel Helper function.
    |
    */
    public function addColumns(): PowerGridEloquent
    {
        return PowerGrid::eloquent()
            ->addColumn('id')
            ->addColumn('fullname')
            ->addColumn('status_name')
            ->addColumn('created_at')
            ->addColumn('created_at_formatted', fn (tmarket_detail $model) => Carbon::parse($model->created_at)->format('d/m/Y H:i:s'));
    }

    /*
    |--------------------------------------------------------------------------
    |  Include Columns
    |--------------------------------------------------------------------------
    | Include the columns added columns, making them visible on the Table.
    | Each column can be configured with properties, filters, actions...
    |
    */

    /**
     * PowerGrid Columns.
     *
     * @return array<int, Column>
     */
    public function columns(): array
    {
        return [

            Column::make('Name', 'fullname')
                ->searchable()
                ->sortable(),
            Column::make('No Hp', 'nohp')
                ->searchable()
                ->sortable(),
            Column::make('City', 'city')
                ->searchable()
                ->sortable(),
            Column::make('Status', 'status_name')
                ->searchable()
                ->sortable(),

            Column::make('Created at', 'created_at')
                ->hidden(),

            Column::make('Created at', 'created_at_formatted', 'created_at')
                ->searchable()
        ];
    }

    /**
     * PowerGrid Filters.
     *
     * @return array<int, Filter>
     */
    public function filters(): array
    {
        return [
            Filter::inputText('fullname'),
            Filter::inputText('nohp'),
            Filter::datepicker('created_at_formatted', 'created_at'),
        ];
    }

    /*
    |--------------------------------------------------------------------------
    | Actions Method
    |--------------------------------------------------------------------------
    | Enable the method below only if the Routes below are defined in your app.
    |
    */

    /**
     * PowerGrid tmarket_detail Action Buttons.
     *
     * @return array<int, Button>
     */


    public function actions(): array
    {

        $cekrol = UserRole::query()->where(['user_id'=>auth()->user()->id])->first();
        if(!empty($cekrol)) {
            return [
                Button::make('process', 'Process')
                    ->target('')
                    ->class('bg-indigo-500 cursor-pointer text-white px-3 py-2.5 m-1 rounded text-sm')
                    ->route('processListTaskTm', ['dvi' => 'datavendor_id']),

//           Button::make('destroy', 'Delete')
//               ->class('bg-red-500 cursor-pointer text-white px-3 py-2 m-1 rounded text-sm')
//               ->route('tmarket_detail.destroy', ['tmarket_detail' => 'id'])
//               ->method('delete')
            ];
        }
        return [];
    }


    /*
    |--------------------------------------------------------------------------
    | Actions Rules
    |--------------------------------------------------------------------------
    | Enable the method below to configure Rules for your Table and Action Buttons.
    |
    */

    /**
     * PowerGrid tmarket_detail Action Rules.
     *
     * @return array<int, RuleActions>
     */

    /*
    public function actionRules(): array
    {
       return [

           //Hide button edit for ID 1
            Rule::button('edit')
                ->when(fn($tmarket_detail) => $tmarket_detail->id === 1)
                ->hide(),
        ];
    }
    */

    public function exportToXLS(){
        return Excel::download(new \App\Exports\AssignTask($this->tmarketid), 'AssignTask.xlsx', \Maatwebsite\Excel\Excel::XLSX);
    }
    public function exportToCsv(){
        return Excel::download(new \App\Exports\AssignTask($this->tmarketid), 'AssignTask.csv', \Maatwebsite\Excel\Excel::CSV);
    }
}
