<?php

namespace App\Http\Livewire\Tmarket\User;

use App\Models\User;
use App\Models\UserRole;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;

class Create extends Component
{
    public $fullname,$email,$password,$password_confirmation;
    public $user;
    public $queryString=['user'];

    protected $rules = [
        "fullname"=>'required|string|max:255',
        'email'         => 'required|string|email|max:255',
        'password'      => 'required|confirmed|min:8',
    ];
    public function mount(){
        if(!empty($this->user)){
            $findUser = User::where(['id'=>$this->user])->first();
            $this->fullname=$findUser->name;
            $this->email=$findUser->email;
        }
    }
    public function render()
    {
        return view('livewire.tmarket.user.create');
    }

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    function save(){
        $this->validate();
        if(empty($this->user)) {
            $createUser = User::create([
                'name' => $this->fullname,
                'email' => $this->email,
                'password' => Hash::make($this->password),
            ]);
            UserRole::create([
                'user_id' => $createUser->id,
                'role' => 'TM'
            ]);
        }
        if(!empty($this->user)){
            User::where(['id'=>$this->user])->update([
                'name' => $this->fullname,
                'email' => $this->email,
                'password' => Hash::make($this->password),
            ]);
        }
        return $this->redirectRoute("listUserTm");
    }
}
