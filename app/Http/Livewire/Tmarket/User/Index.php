<?php

namespace App\Http\Livewire\Tmarket\User;

use Livewire\Component;

class Index extends Component
{
    public function render()
    {
        return view('livewire.tmarket.user.index');
    }
}
