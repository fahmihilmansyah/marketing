<?php

namespace App\Http\Livewire\Tmarket\User\Select;

use App\Models\User;
use App\Models\UserRole;
use Livewire\Component;

class Index extends Component
{
    public $tmId;
    public $listTm;
    protected $queryString=['tmId'];
    public function mount(){
        $userRole = UserRole::query()->select(['user_id'])->get();
        $this->listTm=User::query()->whereIn('id',$userRole)->get()->toArray();

        if($this->tmId) {
//            dd($this->tmId,$userRole, $this->listTm);
        }
    }
    public function render()
    {
        $this->emit('tmId',$this->tmId);
        return view('livewire.tmarket.user.select.index');
    }
}
