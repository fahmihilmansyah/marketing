<?php

namespace App\Http\Livewire\Master\Tmstatus;

use App\Models\master_status;
use Livewire\Component;
use LivewireUI\Modal\ModalComponent;

class Formdata extends ModalComponent
{
    public $statusName;

    protected $rules = [
        "statusName"=>'required|string|max:255',
    ];
    public function render()
    {
        return view('livewire.master.tmstatus.formdata');
    }
    public function save(){
        $this->validate();
        master_status::create(['status_name'=>$this->statusName]);
        notify()->success('Success add data.') ;
        return $this->redirectRoute("listMasterStatus");
    }
}
