<?php

namespace App\Http\Livewire\Dashboard;

use App\Models\master_status;
use App\Models\UserRole;
use Livewire\Component;

class Index extends Component
{
    public $listTask,$startDate,$endDate;
    public $tmId;
    public $roleId;
//    protected $queryString=['tmId'];
    protected $listeners=['tmId'=>'onTmId'];
    function onTmId($tmid){
        $this->tmId = $tmid;
    }
    public function mount(){
        $findUserRole = UserRole::query()->where(['user_id'=>auth()->user()->id])->first();
        if(!empty($findUserRole))
        {
            $this->roleId = $findUserRole;
        }
        $this->startDate = date('Y-m-d');
        $this->endDate = date('Y-m-d');
        $this->listTask = master_status::all();
    }
    function edStartDate($ed){
        $this->startDate=$ed;
    }
    function edEndDate($ed){
        $this->endDate=$ed;
    }
    public function render()
    {
        return view('livewire.dashboard.index');
    }
}
