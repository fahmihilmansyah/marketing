<?php

namespace App\Http\Livewire\Dashboard;

use App\Models\tmarket;
use App\Models\tmarket_detail;
use App\Models\UserRole;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class CardTask extends Component
{
    public $taskid;
    public $tmId;
    public $startDate,$endDate;
    public $queryString=['taskid','taskName','startDate','endDate','tmId'];
    public $taskName,$countTask;
    function mount(){
        $findUserRole = UserRole::query()->where(['user_id'=>auth()->user()->id])->first();

        $findTaskTmarket = tmarket_detail::query()->where(['status_id'=>$this->taskid])->whereBetween(DB::raw('date(updated_at)'),[$this->startDate,$this->endDate])->groupBy(['status_id']);
        if(!empty($findUserRole)){
            $findmarketTm = tmarket::query()->select(['id'])->where(['tm_user_id'=>auth()->user()->id]);
            $findTaskTmarket->whereIn('tmarket_id',$findmarketTm);
        }
        if(empty($findUserRole)){
            $findmarketTm = tmarket::query()->select(['id'])->where(['tm_user_id'=>$this->tmId]);
            $findTaskTmarket->whereIn('tmarket_id',$findmarketTm);
        }
        $sum= $findTaskTmarket->count('status_id');
       $this->countTask=number_format($sum,0,",",".");
    }
    public function render()
    {
        return view('livewire.dashboard.card-task');
    }
}
