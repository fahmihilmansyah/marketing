<?php

namespace App\Http\Livewire\Datavendor\List;

use Livewire\Component;

class Index extends Component
{
    public function render()
    {
        return view('livewire.datavendor.list.index');
    }
}
