<?php

namespace App\Http\Livewire\Datavendor\Generate;

use Livewire\Component;

class Listdata extends Component
{
    public function render()
    {
        return view('livewire.datavendor.generate.listdata');
    }
}
