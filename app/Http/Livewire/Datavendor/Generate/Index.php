<?php

namespace App\Http\Livewire\Datavendor\Generate;

use App\Exports\Datavendor;
use App\Models\master_status;
use App\Models\tmarket;
use App\Models\tmarket_detail;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Ramsey\Uuid\Uuid;

class Index extends Component
{

    public $selectOption,$usertm,$sumtm,$selectStatus,$statusid;
    public function mount(){
        $this->sumtm = 1000;
        $finduser = User::select(['users.*'])->join("user_role",'user_role.user_id','=','users.id')->where(['role'=>'TM'])->get();
        $this->selectOption =$finduser ;
        $this->selectStatus = master_status::all();
    }
    protected $rules=[
        'usertm'=>'required'];
    protected $messages = [
        'usertm.required' => 'Please select user TM.',
    ];
    public function render()
    {
        return view('livewire.datavendor.generate.index');
    }
    function save(){
        $this->validate();
        $findtmarketdetail = DB::table('tmarket_detail')
            ->select(['tmarket_detail.datavendor_id'])
            ->join('tmarket','tmarket.id','=','tmarket_detail.tmarket_id');
        if(!empty($this->statusid))
        {
            $findtmarketdetail->where(['tmarket_detail.status_id'=>$this->statusid]);
            $findDataVendor = DB::table("datavendor")->whereIn("id",$findtmarketdetail)->limit($this->sumtm)->get();
        }else{
            $findtmarketdetail->where(['tmarket.tm_user_id'=>$this->usertm]);
            $findDataVendor = DB::table("datavendor")->whereNotIn("id",$findtmarketdetail)->limit($this->sumtm)->get();
        }
        if (empty($findDataVendor)){
            notify()->error('Error: Task not found') ;
            return $this->redirectRoute("generateListDataTm");
        }
        $tmarket = tmarket::create([
            'tm_user_id'=>$this->usertm,
            'assign_user_id'=>auth()->user()->id
        ]);
        $tmarketid = $tmarket->id;
        $tmpinsrttmarketdetail =[];
        foreach ($findDataVendor as $r){
            $tmpinsrttmarketdetail[]=array(
                'id'=>Uuid::uuid4(),
                'tmarket_id'=>$tmarketid,
                'datavendor_id'=>$r->id,
                'created_at'=>date("Y-m-d H:i:s"),
                'updated_at'=>date("Y-m-d H:i:s")
            );
        }
        tmarket_detail::insert($tmpinsrttmarketdetail);
        notify()->success('Success Assign Task️') ;
        return $this->redirectRoute("generateListDataTm");
//        $findDatavendor = Dat avendor::where()
    }
}
