<?php

namespace App\Http\Livewire\Datavendor;

use Livewire\Component;

class ListDatavendor extends Component
{
    public function render()
    {
        return view('livewire.datavendor.list-datavendor');
    }
}
