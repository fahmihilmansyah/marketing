<?php

namespace App\Http\Livewire\Datavendor;

use App\Imports\DatalengkapImport;
use App\Imports\DatamainImport;
use App\Models\datavendor;
use App\Models\datavendor_lengkap_file;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithFileUploads;

class Updatekelengkapan extends Component
{use WithFileUploads;
    public $photo,$vendorlengkapfileid;
    public $dvi;
    protected $queryString = ['dvi'];
    public function mount(){
        $this->vendorfileid = $this->dvi;
    }
    public function render()
    {
        return view('livewire.datavendor.updatekelengkapan');
    }
    public function save()
    {
        $this->validate([
            'photo' => 'file|required', // 1MB Max
        ]);
        $user_id = auth()->user()->id;
        $getSizeFile = $this->photo->getSize();
        $inputFileName= $this->photo->store('photo');
        $inputFileName = public_path('public'."/".$inputFileName);
//        $inputFileName = './sampleData/example1.xls';

        /** Load $inputFileName to a Spreadsheet Object  **/
//        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($inputFileName);
//        $activesheet = $spreadsheet->getActiveSheet();
        $arrdatavendorfile =array(
            "path_file"=>$inputFileName,
            "size_file"=>$getSizeFile,
            "user_id"=>$user_id
        );
        $datavendorlengkapfile = datavendor_lengkap_file::create($arrdatavendorfile);
        $datavendorlengkapfileid = $datavendorlengkapfile->id;
        $this->vendorlengkapfileid = $datavendorlengkapfileid;

        ini_set('max_execution_time', '0');
        \Maatwebsite\Excel\Facades\Excel::import(new DatalengkapImport($datavendorlengkapfileid),$inputFileName);
//        foreach ($activesheet->toArray() as $k=>$v){
//            if($k>0) {
//                $nohp = $v[8];
//                if (substr($nohp, 0, 1) != '8') {
//                    $nohp = '0' . substr($nohp, 1, 14);
//                }
//                $finddatavendor = datavendor::where(['nohp' => $nohp])->first();
//
//                $arrinput = array(
//                    "fullname" => $v[0],
//                    "nohp" => substr($v[8], 0, 1) != '0' ? '0' . $v[8] : $v[8],
//                    "add1" => $v[1],
//                    "add2" => $v[2],
//                    "add3" => $v[3],
//                    "city" => $v[4],
//                    "zip" => $v[5],
//                    "homeph1" => substr($v[6], 0, 1) != '0' ? '0' . $v[6] : $v[6],
//                    "bussph1" => substr($v[7], 0, 1) != '0' ? '0' . $v[7] : $v[7],
//                    "mobile" => substr($v[8], 0, 1) != '0' ? '0' . $v[8] : $v[8],
//                    "sex" => $v[9],
//                    "dob" => date("Y-m-d",strtotime(str_replace("/","-",$v[10]))),
//                    "datavendor_lengkap_file_id" => $datavendorlengkapfileid,
//                );
//
//                if (empty($finddatavendor)) {
//                    datavendor::create($arrinput);
//                }
//                if (!empty($finddatavendor)) {
//                    $finddatavendor->update($arrinput);
//                }
//            }
//        }
        notify()->success("Submit success");
        return $this->redirectRoute("showDatavendor");
    }
}
