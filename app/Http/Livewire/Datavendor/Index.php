<?php

namespace App\Http\Livewire\Datavendor;

use App\Imports\DatamainImport;
use App\Models\datavendor;
use App\Models\datavendor_file;
use App\Models\datavendor_file_verif;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Livewire\Component;
use Livewire\WithFileUploads;
use Maatwebsite\Excel\Excel;
use Ramsey\Uuid\Uuid;

class Index extends Component
{
    use WithFileUploads;
    public $photo,$vendorfileid;
    public $dvi;
    protected $queryString = ['dvi'];
    public function mount(){
        $this->vendorfileid = $this->dvi;
    }
    public function render()
    {
        return view('livewire.datavendor.index');
    }
    public function save()
    {
        $this->validate([
            'photo' => 'file|required', // 1MB Max
        ]);
        try {

            $user_id = auth()->user()->id;
            $getSizeFile = $this->photo->getSize();
            $inputFileName = $this->photo->store('photo');
            $inputFileName = public_path('public' . "/" . $inputFileName);
//        $inputFileName = './sampleData/example1.xls';

            $arrdatavendorfile = array(
                "path_file" => $inputFileName,
                "size_file" => $getSizeFile,
                "user_id" => $user_id
            );
            $datavendorfile = datavendor_file::create($arrdatavendorfile);
            $datavendorfileid = $datavendorfile->id;
            $this->vendorfileid = $datavendorfileid;
//            Excel::import(new DatamainImport,$inputFileName);
            ini_set('max_execution_time', '0');
            \Maatwebsite\Excel\Facades\Excel::import(new DatamainImport($datavendorfileid),$inputFileName);
            notify()->success('Success Upload data');
            return $this->redirectRoute("uploadDatavendor", ["dvi" => $datavendorfileid]);
        }catch (\Exception $e){
            dd($e->getMessage());
        }
    }
}
