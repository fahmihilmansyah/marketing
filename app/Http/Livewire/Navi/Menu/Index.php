<?php

namespace App\Http\Livewire\Navi\Menu;

use App\Models\UserRole;
use Livewire\Component;

class Index extends Component
{
    public $roleStatus;
    public function mount(){
        $findUserRole = UserRole::query()->where(['user_id'=>auth()->user()->id])->first();
        if(!empty($findUserRole)){
            $this->roleStatus = $findUserRole->role;
        }
    }
    public function render()
    {
        return view('livewire.navi.menu.index');
    }
}
