<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;

class HomeController extends BaseController
{
    public function redirect(){
        if(!empty(auth()->user())){
                return redirect()->route('dashboard');
        }
        return redirect()->route('login');
    }
}
