<?php
namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;

class Datavendor implements FromCollection{
    public $dvi;
    function __construct($dvi){
        $this->dvi=$dvi;
    }
    public function collection()
    {
        return \App\Models\datavendor_file_verif::where(['datavendor_file_id'=>$this->dvi])->get();
    }
}
