<?php
namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;

class AssignTask implements FromCollection{
    public $dvi;
    function __construct($dvi){
        $this->dvi=$dvi;
    }
    public function collection()
    {
        return \App\Models\tmarket_detail::query()
            ->select(['datavendor.nohp','datavendor.fullname','datavendor.city',])
            ->join('datavendor','datavendor.id','=','tmarket_detail.datavendor_id')
            ->where(['tmarket_id'=>$this->dvi])->get();
    }
}
