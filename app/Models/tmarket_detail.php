<?php

namespace App\Models;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tmarket_detail extends Model
{
    use HasFactory;
    use Uuid;
    protected $table = 'tmarket_detail';
    protected $guarded = [];
}
