<?php

namespace App\Models;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class datavendor_lengkap_file extends Model
{
    use HasFactory;
    use Uuid;
    protected $table = 'datavendor_lengkap_file';
    protected $guarded = [];
}
