<?php

namespace App\Models;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class datavendor_file_verif extends Model
{
    use HasFactory;
    use Uuid;
    protected $table = 'datavendor_file_verif';
    protected $guarded = [];
}
