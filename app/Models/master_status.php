<?php

namespace App\Models;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class master_status extends Model
{
    use HasFactory;
    protected $table = 'master_status';
    protected $guarded = [];
}
