<?php

namespace App\Console\Commands;

use App\Models\datavendor_file;
use Illuminate\Console\Command;

class Execexcel extends Command{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'execexcel:generate {billreff}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Proses eksekusi dishbursment ppob';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $billreff = $this->argument('billreff');
        $findFile = datavendor_file::query()->where(['id'=>$billreff])->first();
        $inputFileName = $findFile->path_file;
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($inputFileName);
        $activesheet = $spreadsheet->getActiveSheet();
//        foreach ($activesheet->toArray() as $k => $v) {
//            $arrinput = array(
//                "id" => Uuid::uuid4(),
//                "fullname" => $v[0],
//                "nohp" => substr($v[1], 0, 1) != '0' ? '0' . $v[1] : $v[1],
//                "datavendor_file_id" => $billreff,
//                "created_at" => date("Y-m-d H:i:s"),
//                "updated_at" => date("Y-m-d H:i:s"),
//            );
//            $ins = datavendor::insertOrIgnore($arrinput);
//            $arruplddt = array(
//                "fullname" => $v[0],
//                "nohp" => substr($v[1], 0, 1) != '0' ? '0' . $v[1] : $v[1],
//                "datavendor_file_id" => $billreff,
//                "status" => $ins == 0 ? "Data Found" : "Data Not Found"
//            );
//            datavendor_file_verif::create($arruplddt);
//        }
        $arrhucnk = array_chunk($activesheet->toArray(),500);
//        dd($arrhucnk);
        foreach ($arrhucnk as $v){
            $arrinput = array(
                "id" => Uuid::uuid4(),
                "fullname" => $v[0],
                "nohp" => substr($v[1], 0, 1) != '0' ? '0' . $v[1] : $v[1],
                "datavendor_file_id" => $billreff,
                "created_at" => date("Y-m-d H:i:s"),
                "updated_at" => date("Y-m-d H:i:s"),
            );
            $ins = datavendor::insertOrIgnore($arrinput);
            $ins=0;
            $arruplddt = array(
                "fullname" => $v[0],
                "nohp" => substr($v[1], 0, 1) != '0' ? '0' . $v[1] : $v[1],
                "datavendor_file_id" => $billreff,
                "status" => $ins == 0 ? "Data Found" : "Data Not Found"
            );
            datavendor_file_verif::create($arruplddt);
        }
    }
}
