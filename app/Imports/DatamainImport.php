<?php
namespace App\Imports;

use App\Models\datavendor;
use App\Models\datavendor_file_verif;
use App\Models\User;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Ramsey\Uuid\Uuid;

class DatamainImport implements ToModel, WithChunkReading {
    private $datafileid;
    function __construct($datafileid)
    {
        $this->datafileid=$datafileid;
    }

    /**
     * @param array $row
     *
     * @return datavendor
     */
    public function model(array $row)
    {
        $nohp = substr($row[1], 0, 1) != '0' ? '0' . $row[1] : $row[1];
        $arrinput = array(
            "id" => Uuid::uuid4(),
            "fullname" => $row[0],
            "nohp" => $nohp,
            "datavendor_file_id" => $this->datafileid,
            "created_at" => date("Y-m-d H:i:s"),
            "updated_at" => date("Y-m-d H:i:s"),
        );
        $ins = datavendor::insertOrIgnore($arrinput);
            return new datavendor_file_verif([
                'id' => Uuid::uuid4(),
                "fullname" => $row[0],
                "nohp" => $nohp,
                "datavendor_file_id" => $this->datafileid,
                "status" => $ins == 0 ? "Data Found" : "Data Not Found",
                "created_at" => date("Y-m-d H:i:s"),
                "updated_at" => date("Y-m-d H:i:s"),
            ]);
    }
//    public function batchSize(): int
//    {
//        return 1000;
//    }

    //LIMIT CHUNKSIZE
    public function chunkSize(): int
    {
        return 1000; //ANGKA TERSEBUT PERTANDA JUMLAH BARIS YANG AKAN DIEKSEKUSI
    }
}
