<?php
namespace App\Imports;

use App\Models\datavendor;
use App\Models\datavendor_file_verif;
use App\Models\User;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Ramsey\Uuid\Uuid;

class DatalengkapImport implements ToModel, WithChunkReading {
    private $datalengkapfileid;
    function __construct($datafileid)
    {
        $this->datalengkapfileid=$datafileid;
    }

    /**
     * @param array $row
     *
     * @return datavendor
     */
    public function model(array $row)
    {
        if(!empty($row[9]) && strlen($row[9]) == 1) {
            $nohp = substr($row[8], 0, 1) != '0' ? '0' . $row[8] : $row[8];
            $finddatavendor = datavendor::where(['nohp' => $nohp])->first();
            $arrinput = array(
                "fullname" => $row[0],
                "nohp" => $nohp,
                "add1" => $row[1],
                "add2" => $row[2],
                "add3" => $row[3],
                "city" => $row[4],
                "zip" => $row[5],
                "homeph1" => substr($row[6], 0, 1) != '0' ? '0' . $row[6] : $row[6],
                "bussph1" => substr($row[7], 0, 1) != '0' ? '0' . $row[7] : $row[7],
                "mobile" => $nohp,
                "sex" => $row[9],
                "dob" => date("Y-m-d", strtotime(str_replace("/", "-", $row[10]))),
                "datavendor_lengkap_file_id" => $this->datalengkapfileid,
            );

            if (empty($finddatavendor)) {
                datavendor::create($arrinput);
            }
            if (!empty($finddatavendor)) {
                $finddatavendor->update($arrinput);
            }
            return null;
        }
            return null;
    }
//    public function batchSize(): int
//    {
//        return 1000;
//    }

    //LIMIT CHUNKSIZE
    public function chunkSize(): int
    {
        return 1000; //ANGKA TERSEBUT PERTANDA JUMLAH BARIS YANG AKAN DIEKSEKUSI
    }
}
