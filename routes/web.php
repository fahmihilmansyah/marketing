<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [\App\Http\Controllers\HomeController::class, "redirect"]);

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified'
])->group(function () {
    Route::get('/dashboard', \App\Http\Livewire\Dashboard\Index::class)->name('dashboard');
    Route::get('/datavendor', \App\Http\Livewire\Datavendor\ListDatavendor::class)->name('showDatavendor');
    Route::get('/datavendor/upload/main', \App\Http\Livewire\Datavendor\Index::class)->name('uploadDatavendor');
    Route::get('/datavendor/upload/lengkap', \App\Http\Livewire\Datavendor\Updatekelengkapan::class)->name('uploadDatavendorLengkap');
    Route::get('/datavendor/generate', \App\Http\Livewire\Datavendor\Generate\Index::class)->name('generateDataTm');
    Route::get('/datavendor/generate/list', \App\Http\Livewire\Datavendor\Generate\Listdata::class)->name('generateListDataTm');
    Route::get('/tm/task', \App\Http\Livewire\Tmarket\Task\Index::class)->name('showListTaskTm');
    Route::get('/tm/task/detail', \App\Http\Livewire\Tmarket\Task\Detail::class)->name('detailListTaskTm');
    Route::get('/tm/task/process', \App\Http\Livewire\Tmarket\Task\Processtm::class)->name('processListTaskTm');
    Route::get('/tm/user/list', \App\Http\Livewire\Tmarket\User\Index::class)->name('listUserTm');
    Route::get('/tm/user/create', \App\Http\Livewire\Tmarket\User\Create::class)->name('createUserTm');
    Route::get('/master/status/list', \App\Http\Livewire\Master\Tmstatus\Index::class)->name('listMasterStatus');
    Route::get('/master/status/form', \App\Http\Livewire\Master\Tmstatus\Formdata::class)->name('formMasterStatus');
});
