<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tmarket_detail', function (Blueprint $table) {
            $table->string('id',60)->primary();
            $table->string("tmarket_id",60);
            $table->string("datavendor_id",60);
            $table->text("desc");
            $table->string("status_id",60)->nullable(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tmarket_detail');
    }
};
