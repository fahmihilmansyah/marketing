<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('datavendor', function (Blueprint $table) {
            $table->string('id',60)->primary();
            $table->string('datavendor_file_id',60)->nullable(true);
            $table->string('datavendor_lengkap_file_id',60)->nullable(true);
            $table->string("nohp",25)->unique();
            $table->string("fullname","100");
            $table->text("add1")->nullable(true);
            $table->text("add2")->nullable(true);
            $table->text("add3")->nullable(true);
            $table->string("city",60)->nullable(true);
            $table->string("zip",10)->nullable(true);
            $table->string("homeph1",25)->nullable(true);
            $table->string("bussph1",25)->nullable(true);
            $table->string("mobile",25)->nullable(true);
            $table->string("sex",1)->nullable(true);
            $table->date("dob")->nullable(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('datavendor');
    }
};
