<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('datavendor_file_verif', function (Blueprint $table) {
            $table->string('id',60)->primary();
            $table->string('datavendor_file_id',60);
            $table->string('fullname',100);
            $table->string('nohp',30);
            $table->string('status',30);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('datavendor_file_verif');
    }
};
