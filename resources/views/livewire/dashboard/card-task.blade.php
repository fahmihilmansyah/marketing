<div class="rounded bg-gradient-to-r from-cyan-500 to-blue-500 w-80">
    <h3 class="text-white font-semibold sm:text-md md:text-lg lg:text-xl xl:text-2xl p-4 mb-4">{{$taskName}}</h3>
    <div class="flex justify-end p-4">
        <h2 class="text-white font-semibold sm:text-xl md:text-2xl lg:text-3xl xl:text-5xl">{{$countTask}}</h2>
    </div>
</div>
