<div>
    {{-- Do your work, then step back. --}}
    <div class="mt-4 p-4">
        <div class="flex justify-between mb-6">
            <h3 class="font-semibold sm:text-sm md:text-md lg:text-lg xl:text-xl mb-4">Perform Status</h3>
            <form>
            <div class="flex gap-4">
                @if(!$roleId)
                <livewire:tmarket.user.select.index :tmId="$tmId"/>
                @endif
                <div>
                    <label class="font-semibold">Start Date</label>
                    <input type="date" class="w-full" id="startDates" wire:model="startDate" >
                </div>
                <div>
                    <label class="font-semibold">End Date</label>
                    <input type="date" class="w-full" id="endDates" wire:model="endDate" >
                </div>
            </div>
            </form>
        </div>
        <div class=" gap-4 grid grid-cols-4">
            @foreach($listTask as $k=>$v)
                <livewire:dashboard.card-task :tmId="$tmId" :taskid="$v->id" :wire:key="time().$v->id" :startDate="$startDate" :endDate="$endDate"
                                              :taskName="$v->status_name"/>
            @endforeach
        </div>
    </div>
</div>
