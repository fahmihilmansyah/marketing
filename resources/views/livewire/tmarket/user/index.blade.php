<div>
    <div class="p-4 m-2 bg-white">
        <h3 class="text-2xl font-semibold">List User TM</h3>
        <div class="p-4">
            <a href="{{asset('/tm/user/create')}}" class="rounded bg-indigo-500 p-2 text-white">Add User TM</a>
            <div class="p-4">
                <livewire:tmarket.user.tmuser-table/>
            </div>
        </div>
    </div>
</div>
