<div>
    <label>User TM:</label>
    <select wire:model="tmId" @class(['w-full'])>
        <option>-Select User TM-</option>
        @if(!empty($listTm))
        @foreach($listTm as $r)
            <option value="{{$r['id']}}">{{$r['name']}}</option>
        @endforeach
            @endif
    </select>
</div>
