<div class="p-4 m-4">
    <div class="p-4 bg-white">
        <h3 class="p-4 font-semibold sm:text-sm md:text-md lg:text-lg xl:text-xl ">User TM</h3>
        <div class=""></div>
        <form wire:submit.prevent="save" class="min-h-[420px]">
            <div class=" grid grid-cols-2 gap-4">
            <div>
                <span class="block text-sm font-bold text-gray-700 dark:text-gray-300">Full Name:</span>
                <input type="text" wire:model="fullname" class="rounded-lg w-full shadow-sm border border-gray-300 text-gray-700">
                @error('fullname') <span class="error">{{ $message }}</span> @enderror
            </div>
            <div>
                <span class="block text-sm font-bold text-gray-700 dark:text-gray-300">Email:</span>
                <input type="text" wire:model="email" class="rounded-lg w-full shadow-sm border border-gray-300 text-gray-700">
                @error('email') <span class="error">{{ $message }}</span> @enderror
            </div>
            <div>
                <span class="block text-sm font-bold text-gray-700 dark:text-gray-300">Password:</span>
                <input type="password" wire:model="password" class="rounded-lg w-full shadow-sm border border-gray-300 text-gray-700">
                @error('password') <span class="error">{{ $message }}</span> @enderror
            </div>
            <div>
                <span class="block text-sm font-bold text-gray-700 dark:text-gray-300">Re-Password:</span>
                <input type="password" wire:model="password_confirmation" class="rounded-lg w-full shadow-sm border border-gray-300 text-gray-700">
                @error('password_confirmation') <span class="error">{{ $message }}</span> @enderror
            </div>
            </div>
            <div class="flex justify-end mt-2">
                <button type="submit" class="p-2 rounded dark:bg-gradient-to-bl bg-indigo-600 hover:bg-indigo-400 text-white">Save</button>
            </div>
        </form>
    </div>
</div>
