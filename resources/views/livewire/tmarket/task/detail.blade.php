<div>
    {{-- Stop trying to control. --}}
    <div class="p-4 m-4 bg-white">
        <h3 class="font-semibold sm:text-sm md:text-md lg:text-lg xl:text-xl mb-4 ">Task List Detail</h3>
        <livewire:tmarket.task.tm-detail-task-table tmarketid="{{$tmarket}}"/>
    </div>
</div>
