<div>
    <div class="p-4 m-4 bg-white">
        <h3 class="sm:text-sm md:text-md lg:text-lg xl:text-xl font-semibold mb-4">Detail data</h3>
        <div class="border rounded p-4">
            <div class="grid grid-cols-2 gap-4">
                <div>
                    <label class="font-semibold">Fullname:</label>
                    <span>{{$fullname}}</span>
                </div>
                <div>
                    <label class="font-semibold">Sex:</label>
                    <span>{{$sex}}</span>
                </div>
                <div>
                    <label class="font-semibold">Dob:</label>
                    <span>{{$dob}}</span>
                </div>
                <div>
                    <label class="font-semibold">No Hp:</label>
                    <span>{{$nohp}}</span>
                </div>
                <div>
                    <label class="font-semibold">Home Phone:</label>
                    <span>{{$homeph1}}</span>
                </div>
                <div>
                    <label class="font-semibold">Bussiness Phone:</label>
                    <span>{{$bussph1}}</span>
                </div>
                <div>
                    <label class="font-semibold">City:</label>
                    <span>{{$city}}</span>
                </div>
                <div>
                    <label class="font-semibold">Zip:</label>
                    <span>{{$zip}}</span>
                </div>
                <div>
                    <label class="font-semibold">Address 1:</label>
                    <span>{{$add1}}</span>
                </div>
                <div>
                    <label class="font-semibold">Address 2:</label>
                    <span>{{$add2}}</span>
                </div>
                <div>
                    <label class="font-semibold">Address 3:</label>
                    <span>{{$add3}}</span>
                </div>
            </div>
        </div>
        <form wire:submit.prevent="save">
            <div class="border rounded p-4 mt-2">
                <div class="flex gap-4">
                    <div class="flex-1">
                        <label class="font-semibold">Response:</label>
                        <select wire:model="statusId" class="w-full">
                            <option>- choose -</option>
                            @foreach($listStatus as $r)
                                <option value="{{$r->id}}">{{$r->status_name}}</option>
                            @endforeach
                        </select>
                        @error('statusId')<span class="error">{{ $message }}</span> @enderror
                    </div>
                    <div class="flex-1">
                        <label>Keterangan</label>
                        <textarea class="w-full rounded" wire:model="desc"></textarea>
                    </div>
                    <div class="flex items-center">
                            <button class=" bg-indigo-500 hover:bg-indigo-600 text-white p-2 rounded">Submit</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
