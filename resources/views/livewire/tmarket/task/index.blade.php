<div>
    {{-- Success is as dangerous as failure. --}}
    <div class="p-4 m-2 bg-white">
        <h3 class="font-semibold sm:text-sm md:text-md lg:text-lg xl:text-xl mb-4">List Task TM</h3>
        <livewire:tmarket.task.tm-list-task-table/>
    </div>
</div>
