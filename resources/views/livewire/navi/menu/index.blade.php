
    @if(!$roleStatus)
    <div class="ml-2 relative mt-5">
        <x-dropdown class="p-4">
            <x-slot name="trigger">
                            <span class="inline-flex rounded-md">
                                    <button type="button" class="inline-flex items-center px-3 py-2 border border-transparent text-sm leading-4 font-medium rounded-md text-gray-500 bg-white hover:text-gray-700 focus:outline-none focus:bg-gray-50 active:bg-gray-50 transition ease-in-out duration-150">
                                        Master Data

                                        <svg class="ml-2 -mr-0.5 h-4 w-4" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M19.5 8.25l-7.5 7.5-7.5-7.5" />
                                        </svg>
                                    </button>
                                </span>
            </x-slot>

            <x-slot name="content">
                <x-dropdown-link class="" href="{{ route('showDatavendor') }}" :active="request()->routeIs('showDatavendor')">
                    {{ __('Datavendor') }}
                </x-dropdown-link>
                <x-dropdown-link class="" href="{{ route('generateListDataTm') }}" :active="request()->routeIs('generateListDataTm')">
                    {{ __('Assign Task') }}
                </x-dropdown-link>
                <x-dropdown-link class="" href="{{ route('listUserTm') }}" :active="request()->routeIs('listUserTm')">
                    {{ __('User TM') }}
                </x-dropdown-link>
                <x-dropdown-link class="" href="{{ route('listMasterStatus') }}" :active="request()->routeIs('listMasterStatus')">
                    {{ __('Master Status') }}
                </x-dropdown-link>
            </x-slot>
        </x-dropdown>
    </div>
    @endif
    <div class="ml-2 relative mt-5">
        <x-dropdown class="p-4">
            <x-slot name="trigger">
                            <span class="inline-flex rounded-md">
                                    <button type="button" class="inline-flex items-center px-3 py-2 border border-transparent text-sm leading-4 font-medium rounded-md text-gray-500 bg-white hover:text-gray-700 focus:outline-none focus:bg-gray-50 active:bg-gray-50 transition ease-in-out duration-150">
                                        TM Task

                                        <svg class="ml-2 -mr-0.5 h-4 w-4" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M19.5 8.25l-7.5 7.5-7.5-7.5" />
                                        </svg>
                                    </button>
                                </span>
            </x-slot>

            <x-slot name="content">
                <x-dropdown-link class="" href="{{ route('showListTaskTm') }}" :active="request()->routeIs('showListTaskTm')">
                    {{ __('List Task') }}
                </x-dropdown-link>
            </x-slot>
        </x-dropdown>
    </div>
