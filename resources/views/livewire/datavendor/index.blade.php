<div>
    {{-- The whole world belongs to you. --}}
    <div class="p-4 m-2">
        <div class="p-4 flex justify-center">
            <div class="bg-white p-4">
                <h3 class="border-b text-2xl font-semibold mb-4">Upload Data</h3>
                <form wire:submit.prevent="save">
                    <input type="file" wire:model="photo">
                    <div wire:loading wire:target="photo">Uploading...</div>

                    @error('photo') <span class="error">{{ $message }}</span> @enderror

                    <button type="submit" class="bg-blue-500 text-white p-2 rounded">Upload</button>
                </form>
            </div>
        </div>
    </div>
    @if($vendorfileid)
        <div class="bg-white p-4 m-2 mt-4">
            <h3 class="border-b text-2xl font-semibold">Result Data</h3>
            <livewire:data-vendor-table :dvi="$vendorfileid"/>
        </div>
    @endif
</div>
