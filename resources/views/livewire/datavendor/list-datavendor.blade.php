<div>
    <div class="bg-white p-4 m-4">
        <div class="flex justify-between mb-4">
        <h3 class="font-semibold sm:text-sm md:text-md lg:text-lg xl:text-xl">List Datavendor</h3>
            <div class="flex gap-2">
                <a href="{{route('uploadDatavendor')}}" class="bg-indigo-500 hover:bg-indigo-600 text-white p-2">Upload Data Mentah</a>
                <a href="{{route('uploadDatavendorLengkap')}}" class="bg-indigo-500 hover:bg-indigo-600 text-white p-2">Upload Data Lengkap</a>
            </div>
        </div>
        <div>
            <livewire:datavendor.list.datavendor-table/>
        </div>
    </div>
</div>
