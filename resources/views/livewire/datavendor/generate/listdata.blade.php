<div>
    {{-- To attain knowledge, add things every day; To attain wisdom, subtract things every day. --}}
    <div class="bg-white p-4 m-4">
        <div class="flex justify-between">
        <h3 class="text-2xl font-semibold">List Assign Task</h3>
        <a href="{{asset("datavendor/generate")}}" class="justify-end bg-indigo-500 text-white p-2">Create Assign Task</a>
        </div>
        <div class="mt-4">
        <livewire:list-task-tm-table/>
        </div>
    </div>

</div>
