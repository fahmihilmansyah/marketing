<div>

    {{-- To attain knowledge, add things every day; To attain wisdom, subtract things every day. --}}
    <div class="p-4 flex justify-center">
        <div class="bg-white p-4 m-2 w-1/2">
            <h3 class="font-semibold sm:text-sm md:text-md lg:text-lg xl:text-xl mb-4">Assign Task</h3>
            <form wire:submit.prevent="save">
                <div class="grid grid-cols-1 gap-4">
                    <div>
                        <label>User TM</label>
                        <select required class="rounded w-full" wire:model="usertm">
                            <option>Silakan Pilih</option>
                            @foreach($selectOption as $r)
                                <option value="{{$r['id']}}">{{$r['name']}}</option>
                            @endforeach
                        </select>
                        @error('usertm')
                        <label class="text-red-500">{{$message}}</label>
                        @enderror
                    </div>
                    <div>
                        <label>Response Status</label>
                        <select class="rounded w-full" wire:model="statusid">
                            <option>Belum di verifikasi</option>
                            @foreach($selectStatus as $r)
                                <option value="{{$r['id']}}">{{$r['status_name']}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div>
                        <label>Sum TM Task</label>
                        <input type="number" class="rounded w-full" wire:model="sumtm" min="0">
                    </div>
                </div>
                <div class="flex justify-end mt-2">
                    <button class="rounded bg-indigo-500 text-white p-2">Generate</button>
                </div>
            </form>
            {{$this->emitTo('livewire-toast', 'show', 'Project Added Successfully')}}
        </div>
    </div>
</div>
