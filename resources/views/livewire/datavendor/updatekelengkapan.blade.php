<div>
    {{-- Care about people's approval and you will be their prisoner. --}}
    <div class="p-4 m-2">
        <div class="p-4 flex justify-center">
            <div class="bg-white p-4">
                <h3 class="border-b text-2xl font-semibold mb-4">Upload Data Lengkap</h3>
                <form wire:submit.prevent="save">
                    <input type="file" wire:model="photo">
                    <div wire:loading wire:target="photo">Uploading...</div>

                    @error('photo') <span class="error">{{ $message }}</span> @enderror

                    <button type="submit" class="bg-blue-500 text-white p-2 rounded">Upload</button>
                </form>
                <div class="mt-10 flex justify-center flex-col">
                    <label>Contoh Format:</label><a class="p-2 bg-green-500 rounded text-white flex justify-center" href="{{asset('contoh/Contoh_Data_Lengkap.xlsx')}}">Download Now</a>
                </div>
            </div>
        </div>
    </div>
</div>
