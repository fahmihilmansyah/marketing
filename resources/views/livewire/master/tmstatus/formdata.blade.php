<div>
    {{-- Knowing others is intelligence; knowing yourself is true wisdom. --}}
    <div class="bg-white p-4 m-2 w-full">
        <h3 class="font-semibold sm:text-sm md:text-md lg:text-lg xl:text-xl mb-4">Add/Edit Form</h3>
        <form wire:submit.prevent="save">
            <div>
                <label>Status Name</label>
                <input type="text" wire:model="statusName" class="w-full rounded">
                @error('statusName') <span class="error">{{ $message }}</span> @enderror
            </div>
            <div class="flex justify-end">
                <button class="bg-indigo-500 hover:bg-indigo-600 p-2 text-white mt-2 rounded" type="submit">Submit</button>
            </div>
        </form>
    </div>
</div>
