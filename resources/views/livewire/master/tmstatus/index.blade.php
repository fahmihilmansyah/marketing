<div>
    {{-- Stop trying to control. --}}
    <div class="bg-white p-4 m-2">
        <div class="flex justify-between mb-4">
            <h3 class="font-semibold sm:text-sm dm:text-dm lg:text-lg xl:text-xl mb-4 ">List Task Status</h3>
            <button wire:click="$emit('openModal', 'master.tmstatus.formdata')" class="flex gap-2 bg-indigo-500 hover:bg-indigo-600 text-white p-2">
                <svg class="w-5 h-5" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                    <path strokeLinecap="round" strokeLinejoin="round" d="M12 9v6m3-3H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
                </svg>
                Add Task Status</button>
        </div>
        <livewire:master.tmstatus.list-status-table/>
    </div>
</div>
